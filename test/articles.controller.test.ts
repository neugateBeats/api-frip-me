/* eslint-disable prettier/prettier */
import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from 'src/articles/articles.controller';
import { ArticlesService } from 'src/articles/articles.service';
import { CreateArticleDto } from 'src/articles/dto/create-article.dto';
import { UpdateArticleDto } from 'src/articles/dto/update-article.dto';
import { Article } from 'src/articles/entities/article.entity';

describe('ArticlesController', () => {
  let controller: ArticlesController;
  let service: ArticlesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticlesController],
      providers: [ArticlesService],
    }).compile();

    controller = module.get<ArticlesController>(ArticlesController);
    service = module.get<ArticlesService>(ArticlesService);
  });

  describe('create', () => {
    it('should create an article', async () => {
      // Préparation des données
      const createArticleDto: CreateArticleDto = {
        
      };

      // Mock de la méthode du service
      jest.spyOn(service, 'create').mockResolvedValueOnce({} as Article);

      // Appel de la méthode du contrôleur
      const result = await controller.create(createArticleDto);

      // Vérification des résultats
      expect(result).toBeDefined();
      
    });
  });

  describe('findAll', () => {
    it('should return an array of articles', async () => {
      // Mock de la méthode du service
      jest.spyOn(service, 'findAll').mockResolvedValueOnce([] as Article[]);

      // Appel de la méthode du contrôleur
      const result = await controller.findAll();

      // Vérification des résultats
      expect(result).toEqual([]);
     
    });
  });

  describe('findOne', () => {
    it('should return a single article', async () => {
      // Préparation des données
      const articleId = 'example-article-id';

      // Mock de la méthode du service
      jest.spyOn(service, 'findOne').mockResolvedValueOnce({} as Article);

      // Appel de la méthode du contrôleur
      const result = await controller.findOne(articleId);

      // Vérification des résultats
      expect(result).toBeDefined();
      // Ajoutez ici les assertions supplémentaires pour vérifier les résultats
    });
  });

  describe('update', () => {
    it('should update an article', async () => {
      // Préparation des données
      const articleId = 'example-article-id';
      const updateArticleDto: UpdateArticleDto = {
        // Ajoutez ici les propriétés nécessaires pour mettre à jour l'article
      };

      // Mock de la méthode du service
      jest.spyOn(service, 'update').mockResolvedValueOnce({} as Article);

      // Appel de la méthode du contrôleur
      const result = await controller.update(articleId, updateArticleDto);

      // Vérification des résultats
      expect(result).toBeDefined();
      // Ajoutez ici les assertions supplémentaires pour vérifier les résultats
    });
  });

  describe('remove', () => {
    it('should remove an article', async () => {
      // Préparation des données
      const articleId = 'example-article-id';

      // Mock de la méthode du service
      jest.spyOn(service, 'remove').mockResolvedValueOnce({} as Article);

      // Appel de la méthode du contrôleur
      const result = await controller.remove(articleId);

      // Vérification des résultats
      expect(result).toBeDefined();
      // Ajoutez ici les assertions supplémentaires pour vérifier les résultats
    });
  });
});
