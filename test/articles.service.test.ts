/* eslint-disable prettier/prettier */
import { Model } from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesService } from 'src/articles/articles.service';
import { CreateArticleDto } from 'src/articles/dto/create-article.dto';
import { UpdateArticleDto } from 'src/articles/dto/update-article.dto';
import { Article } from 'src/articles/entities/article.entity';

describe('ArticlesService', () => {
  let articlesService: ArticlesService;
  let articleModel: Model<Article>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticlesService,
        {
          provide: 'ARTICLE_MODEL',
          useValue: {
            find: jest.fn(),
            findById: jest.fn(),
            create: jest.fn(),
            findByIdAndUpdate: jest.fn(),
            deleteOne: jest.fn(),
          },
        },
      ],
    }).compile();

    articlesService = module.get<ArticlesService>(ArticlesService);
    articleModel = module.get<Model<Article>>('ARTICLE_MODEL');
  });

  describe('create', () => {
    it('should create a new article', async () => {
      const createArticleDto: CreateArticleDto = {
        // Définir les valeurs appropriées pour le DTO de création de l'article
      };

      const mockArticle: Article = {
        // Définir les valeurs attendues de l'article créé
      };

      //   jest.spyOn(articleModel, 'create').mockResolvedValue(mockArticle);

      const result = await articlesService.create(createArticleDto);

      expect(articleModel.create).toHaveBeenCalledWith(createArticleDto);
      expect(result).toEqual(mockArticle);
    });
  });

  describe('findAll', () => {
    it('should return an array of articles', async () => {
      const mockArticles: Article[] = [
        // Définir les articles simulés pour le retour attendu
      ];

      jest.spyOn(articleModel, 'find').mockResolvedValue(mockArticles);

      const result = await articlesService.findAll();

      expect(articleModel.find).toHaveBeenCalled();
      expect(result).toEqual(mockArticles);
    });
  });


});
