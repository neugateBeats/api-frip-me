/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { StoresService } from './stores.service';
import { StoresController } from './stores.controller';
import { storesProviders } from './stores.providers';
import { DatabaseModule } from 'src/database/database.module';


@Module({
  imports: [DatabaseModule], 
  controllers: [StoresController],
  providers: [
    StoresService,
    ...storesProviders,
  
  ],
})
export class StoresModule {}
