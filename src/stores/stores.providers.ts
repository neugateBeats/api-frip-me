/* eslint-disable prettier/prettier */
import { Connection } from 'mongoose';
import { StoreSchema } from 'src/schemas/store.schema';

export const storesProviders = [
  {
    provide: 'STORE_MODEL',
    useFactory: (connection: Connection) => connection.model('Store', StoreSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];