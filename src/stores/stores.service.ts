/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Store } from './entities/store.entity';
import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';

@Injectable()
export class StoresService {
  async remove(id: string): Promise<any> {
    return this.storeModel.deleteOne({_id: id}).exec();
  }
  
  async update(id: string, updateStoreDto: UpdateStoreDto): Promise<Store> {
    return this.storeModel.findByIdAndUpdate(id, updateStoreDto, { new: true }).exec();
  }
  constructor(
    @Inject('STORE_MODEL')
    private storeModel: Model<Store>,
  ) {}

  async create(createStoreDto: CreateStoreDto): Promise<Store> {
    const createdStore = new this.storeModel(createStoreDto);
    return createdStore.save();
  }

  async findAll(): Promise<Store[]> {
    return this.storeModel.find().exec();
  }

  async findOne(id: string): Promise<Store> {
    return this.storeModel.findById(id).exec();
  }
}
