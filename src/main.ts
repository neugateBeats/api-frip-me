import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('LES COLLECTIONS')
    .setDescription('The API description')
    .setVersion('1.0')
    // .addTag('Articles')
    // .addTag('Stores')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();

// import { NestFactory } from '@nestjs/core';
// import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
// import { AppModule } from './app.module';
// import userRoutes from './userRoutes';

// async function bootstrap() {
//   const app = await NestFactory.create(AppModule);

//   const options = new DocumentBuilder()
//     .setTitle('LES COLLECTIONS')
//     .setDescription('The API description')
//     .setVersion('1.0')
//     // .addTag('Articles')
//     // .addTag('Stores')
//     .addBearerAuth()
//     .build();
//   const document = SwaggerModule.createDocument(app, options);
//   SwaggerModule.setup('api', app, document);

//   app.use('/api/users', userRoutes); // Montez les routes utilisateur

//   await app.listen(3000);
//   console.log(`Application is running on: ${await app.getUrl()}`);
// }

// bootstrap();
