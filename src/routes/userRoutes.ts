/* eslint-disable prettier/prettier */
import express from 'express';
import registerUser from 'src/register/register.controller';


const router = express.Router();
router.post('/register', registerUser);
export default router;