/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Article } from './entities/article.entity';
import { Article as ArticleEntity } from './entities/article.entity';

@ApiBearerAuth() // Authentification requise pour accéder à cette API
@ApiTags('articles') // Étiquette pour regrouper les endpoints liés aux articles
@Controller('articles') // Contrôleur pour gérer les requêtes liées aux articles
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}
  
// Crée un nouvel article
  @Post()
  @ApiOperation({ summary: 'Créer un article' }) 
  @ApiResponse({ status: 403, description: 'Interdit.' }) 
  async create(@Body() createArticleDto: CreateArticleDto): Promise<Article> {
    return this.articlesService.create(createArticleDto);
  }

  // Récupère tous les articles
  @Get()
  findAll(): Promise<Article[]> {
    return this.articlesService.findAll();
  }
// Récupère un article spécifique par son identifiant
  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'L\'enregistrement trouvé',
    type: ArticleEntity,
  })
  findOne(@Param('id') id: string): Promise<Article> {
    return this.articlesService.findOne(id);
  }
// Met à jour un article spécifique par son identifiant
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateArticleDto: UpdateArticleDto) {
    return this.articlesService.update(id, updateArticleDto);
  }
  // Supprime un article spécifique par son identifiant
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.articlesService.remove(id);
  }
}


