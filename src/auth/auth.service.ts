/* eslint-disable prettier/prettier */
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  findUserByUsername: any;
  hashingService: any;
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService
  ) {}

  async signIn(username: string, password: string) {
    const user = await this.usersService.findUserByUsername(username);
    if (!user) {
      throw new UnauthorizedException('Nom d\'utilisateur ou mot de passe invalide');
    }
  
    const isMatch = await this.hashingService.comparePassword(password, user.password);
    if (!isMatch) {
      throw new UnauthorizedException('Nom d\'utilisateur ou mot de passe invalide');
    }
  
    const payload = { username: user.username, sub: user.id };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
  