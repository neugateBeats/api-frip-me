/* eslint-disable prettier/prettier */
import { Body, Controller, Post, HttpCode, HttpStatus, UseGuards, Get, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { Request } from '@nestjs/common';
import { HashingService } from 'src/hashing/hashing.service';


@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService, private hashingService: HashingService) {}

  @HttpCode(HttpStatus.OK)
  @Post('login')
  async signIn(@Body() signInDto: Record<string, any>) {
    const { username, password } = signInDto;

    if (!password) {
      throw new UnauthorizedException('Veuillez fournir un mot de passe');
    }

    // Vérifier si l'utilisateur existe
    const user = await this.authService.findUserByUsername(username);
    if (!user) {
      throw new UnauthorizedException('Nom d\'utilisateur ou mot de passe invalide');
    }

    // Vérifier si le mot de passe est correct
    const isMatch = await this.hashingService.comparePassword(password, user.password);
    if (!isMatch) {
      throw new UnauthorizedException('Nom d\'utilisateur ou mot de passe invalide');
    }

    // Générer le token JWT et renvoyer la réponse
    return this.authService.signIn(username, password);
  }

  @UseGuards(AuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
}