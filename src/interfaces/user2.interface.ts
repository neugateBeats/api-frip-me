/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable prettier/prettier */
import { Document } from 'mongoose';

export interface User2 extends Document {
  
    readonly name: String,
    readonly firstname: String,
    readonly birth_date: Date,
    readonly address: String,
    readonly zipcode: String,
    readonly city: String,
    readonly phone: String,
    readonly email: String,
    readonly password: String,
    readonly picture: String,
}