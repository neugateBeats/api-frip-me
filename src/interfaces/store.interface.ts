/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable prettier/prettier */
import { Document } from 'mongoose';

export interface Store extends Document {
  
  readonly name: String,
  readonly address: String,
  readonly zipcode: String,
  readonly city: String,
  readonly activity: String,
  readonly phone: String,
  readonly picture: String,
  readonly description: String,
  readonly latitude: String,
  readonly longtitude: String,
}