/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Users2 extends Document {
  static findOne(arg0: { email: any; }) {
      throw new Error('Method not implemented.');
  }
  @Prop({ required: true })
  password: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  userId: string;

  @Prop({ required: true })
  phone: string;

  @Prop({ required: true })
  city: string;

  @Prop({ required: true })
  zipcode: string;

  @Prop({ required: true })
  address: string;

  @Prop({ required: true })
  birth_date: Date;

  @Prop({ required: true })
  firstname: string;
}

export const Users2Schema = SchemaFactory.createForClass(Users2);

