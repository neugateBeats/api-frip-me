/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';

export class LoginUser2Dto {
  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;
}
