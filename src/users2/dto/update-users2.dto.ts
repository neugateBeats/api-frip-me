import { PartialType } from '@nestjs/swagger';
import { CreateUsers2Dto } from './create-users2.dto';

export class UpdateUsers2Dto extends PartialType(CreateUsers2Dto) {}
