/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { Users2Service } from './users2.service';
import { CreateUsers2Dto } from './dto/create-users2.dto';
import { UpdateUsers2Dto } from './dto/update-users2.dto';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Users2 } from './entities/users2.entity';
import { Users2 as Users2Entity } from './entities/users2.entity';
import { LoginUser2Dto } from './dto/LoginUser2Dto';
import { AuthResponseDto } from './dto/AuthResponseDto';


@ApiBearerAuth()
@ApiTags('Users2')
@Controller('Users2')
export class Users2Controller {
  constructor(private readonly users2Service: Users2Service) {}

  @Post()
  @ApiOperation({ summary: 'Create User' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(@Body() createUser2Dto: CreateUsers2Dto): Promise<Users2> {
    return this.users2Service.create(createUser2Dto);
  }
  // Route pour la connexion des utilisateurs
  @Post('login')
@ApiOperation({ summary: 'Users2 login' })
@ApiResponse({ status: 200, description: 'Authenticated successfully' })
@ApiResponse({ status: 401, description: 'Unauthorized' })
async login(@Body() loginUser2Dto: LoginUser2Dto): Promise<AuthResponseDto> {
  // Vérifiez les informations d'identification de l'utilisateur et générer un token d'authentification
  const authResponse = await this.users2Service.login(loginUser2Dto);
  return authResponse;
}
// Récupère tous les enregistrements
@Get()
findAll(): Promise<Users2[]> {
return this.users2Service.findAll();
}
// Récupère un enregistrement spécifique par son identifiant
@Get(':id')
@ApiResponse({
status: 200,
  description: 'The found record',
  type: Users2Entity,
})
findOne(@Param('id') id: string) {
  return this.users2Service.findOne(id);
}
// Met à jour un enregistrement spécifique par son identifiant
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUsers2Dto: UpdateUsers2Dto) {
    return this.users2Service.update(id, updateUsers2Dto);
  }
// Supprime un enregistrement spécifique par son identifiant
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.users2Service.remove(id);
  }
}
