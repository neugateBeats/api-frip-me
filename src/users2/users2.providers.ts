/* eslint-disable prettier/prettier */
import { Connection } from 'mongoose';
import { User2Schema } from 'src/schemas/user2.schema';

export const users2Providers = [
  {
    provide: 'USER_MODEL',
    useFactory: (connection: Connection) => connection.model('User', User2Schema),
    inject: ['DATABASE_CONNECTION'],
  },
];