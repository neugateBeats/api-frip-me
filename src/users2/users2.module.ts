/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { Users2Service } from './users2.service';
import { Users2Controller } from './users2.controller';
import { users2Providers } from './users2.providers';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule], 
  controllers: [Users2Controller],
  providers: [
    Users2Service,
    ...users2Providers,
  ],
})
export class Users2Module {}
