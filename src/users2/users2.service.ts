/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Model } from 'mongoose';
import { Injectable, Inject, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { Users2 } from './entities/users2.entity';
import { CreateUsers2Dto } from './dto/create-users2.dto';
import { UpdateUsers2Dto } from './dto/update-users2.dto';
import { LoginUser2Dto } from './dto/LoginUser2Dto';
import { AuthResponseDto } from './dto/AuthResponseDto';
import * as bcrypt from 'bcrypt'; 
import * as jwt from 'jsonwebtoken';
  

// @Injectable()
// export class Users2Service {
//   User2Model: any;
//   login: any;
//   async remove(id: string): Promise<any> {
//     return this.user2Model.deleteOne({_id: id}).exec();
//   }


//   async update(id: string, updateUser2Dto: UpdateUsers2Dto): Promise<Users2> {
//     return this.user2Model.findByIdAndUpdate(id, updateUser2Dto, { new: true }).exec();
//   }
//   constructor(
//     @Inject('USER_MODEL')
//     private user2Model: Model<Users2>,
//   ) {}

//   async create(createUser2Dto: CreateUsers2Dto): Promise<Users2> {
//     const createdUser2 = new this.user2Model(createUser2Dto);
//     return createdUser2.save();
//   }

//   async findAll(): Promise<Users2[]> {
//     return this.User2Model.find().exec();
//   }

//   async findOne(id: string): Promise<Users2> {
//     return this.user2Model.findById(id).exec();
//   }
// }


@Injectable()
export class Users2Service {
  User2Model: any;

  constructor(
    @Inject('USER_MODEL')
    private user2Model: Model<Users2>,
  ) {}

  async create(createUser2Dto: CreateUsers2Dto): Promise<Users2> {
    const createdUser2 = new this.user2Model(createUser2Dto);
    return createdUser2.save();
  }

  async findAll(): Promise<Users2[]> {
    return this.user2Model.find().exec();
  }

  async findOne(id: string): Promise<Users2> {
    return this.user2Model.findById(id).exec();
  }

  async login(loginUser2Dto: LoginUser2Dto): Promise<AuthResponseDto> {
    const { email, password } = loginUser2Dto;

    // Recherchez l'utilisateur dans la base de données en utilisant l'adresse e-mail
    const user = await this.user2Model.findOne({ email }).exec();

    if (!user) {
      throw new NotFoundException('Utilisateur introuvable');
    }

    // Vérifiez si le mot de passe saisi correspond au mot de passe enregistré dans la base de données
    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      throw new UnauthorizedException('Mot de passe incorrect');
    }

    // Générez un token d'authentification (utilisez une bibliothèque appropriée pour cela)
    const token = generateAuthToken(user); // Remplacez cette fonction par votre logique de génération de token

    // Créez l'objet AuthResponseDto contenant les résultats de l'authentification
    const authResponse: AuthResponseDto = {
      token,
    };

    return authResponse;
  }
  

  async remove(id: string): Promise<any> {
    return this.user2Model.deleteOne({ _id: id }).exec();
  }

  async update(id: string, updateUser2Dto: UpdateUsers2Dto): Promise<Users2> {
    return this.user2Model.findByIdAndUpdate(id, updateUser2Dto, { new: true }).exec();
  }
}

const token = jwt.sign({ userId: Users2 }, 'votre_clé_secrète');
// function generateAuthToken(users2: import("mongoose").Document<unknown, {}, Users2> & Omit<Users2 & { _id: import("mongoose").Types.ObjectId; }, never>) {
//   throw new Error('Function not implemented.');
  

//   return token;
// 

function generateAuthToken(user: import("mongoose").Document<unknown, {}, Users2> & Omit<Users2 & { _id: import("mongoose").Types.ObjectId; }, never>): string {
  const token = jwt.sign({ userId: user._id }, 'votre_clé_secrète');
  return token;
}

