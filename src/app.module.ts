/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticlesModule } from './articles/articles.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { StoresModule } from './stores/stores.module';
import { Users2Module } from './users2/users2.module';
import { BrandsModule } from './brands/brands.module';
import { CategorysModule } from './categorys/categorys.module';
import { OrdersModule } from './orders/orders.module';
import { PaymentsModule } from './payments/payments.module';
import { CommentsModule } from './comments/comments.module';
import { MessagesModule } from './messages/messages.module';
import { EncryptionService } from './encryption/encryption.service';
import { HashingService } from './hashing/hashing.service';

@Module({
  imports: [ArticlesModule, StoresModule, AuthModule, UsersModule, StoresModule, Users2Module, BrandsModule, CategorysModule, OrdersModule, PaymentsModule, CommentsModule, MessagesModule],
  controllers: [AppController],
  providers: [AppService, EncryptionService, HashingService],
})
export class AppModule {}
