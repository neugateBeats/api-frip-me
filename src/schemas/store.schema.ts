/* eslint-disable prettier/prettier */
import * as mongoose from 'mongoose';

export const StoreSchema = new mongoose.Schema({
  name: String,
  address: String,
  zipcode: String,
  city: String,
  activity: String,
  phone: String,
  picture: String,
  description: String,
  latitude: { type: Number, required: true },
  longitude: { type: Number, required: true },
});