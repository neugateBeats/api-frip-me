/* eslint-disable prettier/prettier */
import * as mongoose from 'mongoose';

export const User2Schema = new mongoose.Schema({
  name: { type: String, required: true },
  firstname: { type: String, required: true },
  birth_date: { type: Date, required: true, match: /^\d{4}-\d{2}-\d{2}$/ },
  address: { type: String, required: true },
  zipcode: { type: String, required: true },
  city: { type: String, required: true },
  phone: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  picture: { type: String }
});