/* eslint-disable prettier/prettier */
import * as mongoose from 'mongoose';

export const ArticleSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  size: { type: Number, required: true },
  price: { type: Number, required: true },
  condition: { type: String, required: true },
  publication_date: {
    type: Date,
    get: function(date) {
      return date.toISOString().slice(0, 10);
    }
  },
  picture: { type: String, required: true },
});