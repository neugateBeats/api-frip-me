/* eslint-disable prettier/prettier */
import { Request, Response } from 'express';
import { Users2 } from 'src/users2/entities/users2.entity';

const registerUser = async (req: Request, res: Response) => {
  try {
    const {
      email,
      password,
      firstName,
      lastName,
      phoneNumber,
      day,
      month,
      year,
      country,
      city,
      postalCode,
      address,
      apartment,
      floor,
      door,
      landmark,
    } = req.body;

    // Vérifiez si l'utilisateur existe déjà dans la base de données
    const existingUser = await Users2.findOne({ email });
    if (existingUser!== null) {
      return res.status(400).json({ message: "L'utilisateur existe déjà" });
    }

    // Créez un nouvel utilisateur avec les données d'inscription
    const newUser = new Users2({
      email,
      password,
      firstName,
      lastName,
      phoneNumber,
      day,
      month,
      year,
      country,
      city,
      postalCode,
      address,
      apartment,
      floor,
      door,
      landmark,
    });

    // Enregistrez le nouvel utilisateur dans la base de données
    await newUser.save();

    // Répondez avec succès et renvoyez les informations d'utilisateur si nécessaire
    return res.status(201).json({ message: "Inscription réussie" });
  } catch (error) {
    // En cas d'erreur, renvoyez un message d'erreur approprié
    return res.status(500).json({ message: "Une erreur s'est produite lors de l'inscription de l'utilisateur" });
  }
};

export default registerUser;
